#ifndef VECTORMESONDECAYS_H
#define VECTORMESONDECAYS_H

#include "Pythia8/ParticleDecays.h"
#include "PrimordialKT.h"
namespace Pythia8{

// External Fortran routines.
// Routine for the polarized decay VM -> PS + PS.
extern "C" void __routines_MOD_vmtopspsdecay(double *Px, double *Py, double *Pz, double *E,
                                             double *M, double *M1, double *M2,
                                             double *Sx, double *Sy, double *Sz,
                                             double *kpx, double *kpy,
                                             double *P1x, double *P1y, double *P1z, double *E1,
                                             double *P2x, double *P2y, double *P2z, double *E2,
                                             double *Dxx, double *Dxy, double *Dxz,
                                             double *Dyx, double *Dyy, double *Dyz,
                                             double *Dzx, double *Dzy, double *Dzz);
// Routine for the polarized decay VM -> PS + V.
extern "C" void __routines_MOD_vmtopsvdecay(double *Px, double *Py, double *Pz, double *E,
                                            double *M, double *M1, double *M2,
                                            double *Sx, double *Sy, double *Sz,
                                            double *kpx, double *kpy,
                                            double *P1x, double *P1y, double *P1z, double *E1,
                                            double *P2x, double *P2y, double *P2z, double *E2,
                                            double *Dxx, double *Dxy, double *Dxz,
                                            double *Dyx, double *Dyy, double *Dyz,
                                            double *Dzx, double *Dzy, double *Dzz);
// Routine for the polarized decay VM -> PS + PS + PS.
extern "C" void __routines_MOD_vmtopspspsdecay(double *Px, double *Py, double *Pz, double *E,
                                               double *M, double *M1, double *M2,
                                               double *Sx, double *Sy, double *Sz,
                                               double *kpx, double *kpy,
                                               double *P1x, double *P1y, double *P1z, double *E1,
                                               double *P2x, double *P2y, double *P2z, double *E2,
                                               double *P3x, double *P3y, double *P3z, double *E3,
                                               double *Dxx, double *Dxy, double *Dxz,
                                               double *Dyx, double *Dyy, double *Dyz,
                                               double *Dzx, double *Dzy, double *Dzz);

// Routine for the calculation of q' polarization vector when a hadron is emitted.
extern "C" void __routines_MOD_propagatespin(double *KTPRIMX, double *KTPRIMY,
                                             double *SXX, double *SYY, double *SZZ, int *HADRON,
                                             double *SOUTXX, double *SOUTYY, double *SOUTZZ);

// Class to save the decayer and its daughters.
class Decayer {

  public:
  // Empty constructor.
  Decayer(){};

  // Constructor.
  Decayer(Particle decayer, bool fromPosin, int iRin, int iLin)
    : Mother(decayer), fromPos(fromPosin) {
      if(fromPos) {iRight = iRin; iLeft = -1;}
      else {iRight=-1; iLeft = iLin;}
    }

  // Add a particle to the list of daughters.
  void setDaughter(Particle& Daughterin){
    Daughters.push_back(Daughterin);
  }

  // Clear list of daughters.
  void clear(){ Daughters.clear(); }

  // Set the decay status of the decayer.
  void decayed(bool decayedin){
    isDecayed = decayedin;
  }

  // Functions for output.
  bool hasDecayed(){return isDecayed;};
  int iR(){return iRight;}
  int iL(){return iLeft;}
  bool isFromPos(){return fromPos;}

  // Info on the decayer.
  Particle Mother;

  // List of daughters.
  vector<Particle> Daughters;

  private:
  // True if the decayer is produced from the
  // positive string end.
  bool fromPos;
  
  // Indices to trace after how many steps from
  // the positive end (iRight) and negative end (iLeft)
  // the decayer has been produced.
  int iRight;
  int iLeft;

  // True if decayer has been decayed externally.
  bool isDecayed;
};

// Class to save all externally decayed particles.
class SavedHadrons{

  public:
  // Empty constructor.
  SavedHadrons(){};

  // Constructor.
  SavedHadrons(int nFragin, int idMother1in, int idMother2in,
    Vec4 pMother1in, Vec4 pMother2in){
      nFrag = nFragin-1;
      offset = nFragin;
      idMother1 = idMother1in;
      idMother2 = idMother2in;
      pMother1 = pMother1in;
      pMother2 = pMother2in;
      handled = false;
      iL = 0;
      iR = 0;
      Decayers.clear();
    }

  // Add a decayer to the list of decayers.
  void append(shared_ptr<Decayer> decayer, int fromPos){
    Decayers.push_back(decayer);
    increment(fromPos);
  }

  // Clear list of decayers.
  void clear(){
    Decayers.clear();
    iL = 0;
    iR = 0;
    nFrag = offset;  
  }

  // Increment internal indices needed to reconstruct
  // the position of the decayer in the event record.
  void increment(int fromPos){ 
    nFrag += 1;
    if(fromPos) iR += 1;
    else iL +=1;
  }

  // Methods to read off the index of the last parton
  // produced at the parton level and the number of 
  // fragmented hadrons.
  int iOffset(){return offset;}
  int iFrag(){return nFrag;}

  // List with all decayers.
  vector< shared_ptr<Decayer> > Decayers;

  // Indices.
  int nFrag;
  int offset;
  int iL;
  int iR;
  
  // Information on the strind ends.
  int idMother1;
  int idMother2;
  Vec4 pMother1;
  Vec4 pMother2;

  // Flag to tell if the string has already been handled once.
  int handled = 0;
};


// Class for external polarized decays.
class PolarizedDecayHandler:
    public DecayHandler,
    public std::enable_shared_from_this<PolarizedDecayHandler> {

public:

  // Empty constructor.
  PolarizedDecayHandler(){};
  
  // Setup and plug into pyhtia.
  void plugInto(Pythia& pytin, vector<int> possibleDecays_in) {

    //Fill vector with possible decays.
    for(int i=0;i<possibleDecays_in.size();i++)
      possibleDecays.push_back(possibleDecays_in[i]);

    //Set pointer of DecayHandler class to the Pythia object to activate external decays.
    pytin.setDecayPtr(shared_from_this(),possibleDecays);

    //ParticleData class needed to get access to internal Pythia decay channels.
    particleData = &pytin.particleData;

    // Store the event record for debug.  
    storedEvent = pytin.event;

  }

  // To switch off decay chains.
  virtual bool doChainDecay(int idMother){return false;}
  
  //Function to recognize a pseudoscalar meson.
  bool isPseudoScalar(int id_in){
    bool isPS = ((abs(id_in)%10) == 1)? true:false;
    return isPS;
   }

  // Function to recognize a vector meson;
  bool isVectorMeson(int id_in){
    bool isVM = ((abs(id_in)%10) == 3)? true:false;
    return isVM;
  }

  // Saved decay hadrons;
  SavedHadrons savedHadrons;

  // Check whether the picked decay channel can be handled externally.
  int canBeHandled(const Particle&p, DecayChannel& channel){
    int multiplicity = channel.multiplicity();
      if(multiplicity == 2){
          int prod1 = channel.product(0);
          int prod2 = channel.product(1);
          // Case VM -> PS + PS.
          if(isPseudoScalar(prod1) && isPseudoScalar(prod2))
              return 0;
          // Special case VM -> K0S + K0L
          if((prod1==130&&prod2==310)||(prod1==310&&prod2==130))
              return 0;
          // Case VM -> PS + gamma.
          // Special case for rho0 -> eta + gamma
          if(p.id()==113 && prod1==221 && prod2==22)
            return -1;
          if((isPseudoScalar(prod1) && prod2==22) ||
             (isPseudoScalar(prod2) && prod1==22))
              return 1;
          NH_prod1 = prod1;
          NH_prod2 = prod2;
          return -1;
      }
      // Case VM -> PS + PS + PS.
      if(multiplicity == 3 && (p.id()==223 || p.id()==333)) { // Works only for omega/phi->3pi.
        int prod1 = channel.product(0);
        int prod2 = channel.product(1);
        int prod3 = channel.product(2);
        if(isPseudoScalar(prod1) && isPseudoScalar(prod2) && isPseudoScalar(prod3))
          return 2;
        // If not handable decay.
        NH_prod1 = prod1;
        NH_prod2 = prod2;
        NH_prod3 = prod3;
        return -1;
      }
    return -1;
  }

  // Main function to perform the external polarized decay.
  bool doPolarizedDecay(Particle decayer_in, Vec4& Sq, Vec4& kTprim,
                        double DecayMatrix[3][3], RotBstMatrix& GNS_in, bool fromPos){
                        
    // ParticleDataEntry of the decayer.
    auto decDataPtr_in = particleData->findParticle(decayer_in.id());
    decayer_in.setPDEPtr(decDataPtr_in);
    ParticleDataEntry* decDataPtr = &decayer_in.particleDataEntry();

    // Construct decayer object.
    auto decayer = make_shared<Decayer>(decayer_in, fromPos, savedHadrons.iR, savedHadrons.iL);

    // Pick a decay channel by using the Pythia machinery, then decide what to do.
    // Prepare pick.
    if(!decDataPtr->preparePick(decayer_in.id(), decayer_in.m())) {
      cout << "\n StringSpinner::ExternalDecay: preparePick didn't work.\n";
      decayer->decayed(false);
      savedHadrons.append(decayer,fromPos);
      return false;
    }
    // Pick a decay channel.
    DecayChannel& channel = decDataPtr->pickChannel();
    
    // Now check if the decay can be handled.
    NH_prod1 = 0;
    NH_prod2 = 0;
    NH_prod3 = 0;
    int decayType = canBeHandled(decayer_in, channel);

    // If the decay is not handable return false and let Pythia do it.
    if(decayType == -1) {
      decayer->decayed(false);
      savedHadrons.append(decayer,fromPos);
      return false;
    }

    // Set up the relevant information for the simulation of the decay.
    // Momentum of decayer in the string rest frame.
    Vec4 p = GNS_in*decayer_in.p();
    RotBstMatrix Rend = (fromPos == true)? posBoost : negBoost;
    stringRot.Rotate(p);
    p = Rend*p;
    stringRot.RotateBack(p);

    double Px = p.px();
    double Py = p.py();
    double Pz = p.pz();
    double E  = p.e();

    // Masses of decayer and daughters.
    double M = decayer_in.m() ;
    double M1 = particleData->m0(channel.product(0));
    double M2 = particleData->m0(channel.product(1));
    double M3 = (decayType==2)? particleData->m0(channel.product(2)):0.0;

    // Polarization of the fragmenting quark.
    double Sqx = Sq.px();
    double Sqy = Sq.py();
    double Sqz = Sq.pz();

    // Transverse momentum of q'.
    double kpx = kTprim.px();
    double kpy = kTprim.py();

    // Variables needed to communicate with the external Fortran routines:
    //i. 4-momentua of daugheters.
    double P1x, P1y, P1z, E1;
    double P2x, P2y, P2z, E2;
    double P3x, P3y, P3z, E3;

    //ii. Components of the decay matrix.
    double Dxx, Dxy, Dxz;
    double Dyx, Dyy, Dyz;
    double Dzx, Dzy, Dzz;

    // Switch to tell whether the decay was correctly simulated.
    bool done = false;

    // Decay hadrons momenta.
    Vec4 P1, P2, P3;
    P1 = P2 = P3 = Vec4();

    // Decays of type VM -> PS + PS.
    if(decayType == 0 ){
        // Generate VM -> PS + PS polarized decay.
        __routines_MOD_vmtopspsdecay(&Px, &Py, &Pz, &E,
                                     &M, &M1, &M2,
                                     &Sqx, &Sqy, &Sqz,
                                     &kpx, &kpy,
                                     &P1x, &P1y, &P1z, &E1,
                                     &P2x, &P2y, &P2z, &E2,
                                     &Dxx, &Dxy, &Dxz,
                                     &Dyx, &Dyy, &Dyz,
                                     &Dzx, &Dzy, &Dzz);

        // Fill daughters momenta.
        P1 = Vec4(P1x,P1y,P1z,E1);
        P2 = Vec4(P2x,P2y,P2z,E2);

        // Fill decay matrix.
        DecayMatrix[0][0] = Dxx;
        DecayMatrix[0][1] = Dxy;
        DecayMatrix[0][2] = Dxz;
        DecayMatrix[1][0] = Dyx;
        DecayMatrix[1][1] = Dyy;
        DecayMatrix[1][2] = Dyz;
        DecayMatrix[2][0] = Dzx;
        DecayMatrix[2][1] = Dzy;
        DecayMatrix[2][2] = Dzz;
        // Done.
        done = true;

    // Decays of type VM -> PS + V.
    } else if (decayType == 1) {
        // Generate VM -> PS + V polarized decay.
        __routines_MOD_vmtopsvdecay(&Px, &Py, &Pz, &E,
                                    &M, &M1, &M2,
                                    &Sqx, &Sqy, &Sqz,
                                    &kpx, &kpy,
                                    &P1x, &P1y, &P1z, &E1,
                                    &P2x, &P2y, &P2z, &E2,
                                    &Dxx, &Dxy, &Dxz,
                                    &Dyx, &Dyy, &Dyz,
                                    &Dzx, &Dzy, &Dzz);
        // Fill daughters momenta.
        P1 = Vec4(P1x,P1y,P1z,E1);
        P2 = Vec4(P2x,P2y,P2z,E2);
        // Fill decay matrix.
        DecayMatrix[0][0] = Dxx;
        DecayMatrix[0][1] = Dxy;
        DecayMatrix[0][2] = Dxz;
        DecayMatrix[1][0] = Dyx;
        DecayMatrix[1][1] = Dyy;
        DecayMatrix[1][2] = Dyz;
        DecayMatrix[2][0] = Dzx;
        DecayMatrix[2][1] = Dzy;
        DecayMatrix[2][2] = Dzz;
        // Done.
        done = true;

      // Decays of type VM -> PS + PS + PS.
      } else if (decayType == 2){
          // Generate VM -> PS + PS + PS polarized decay.
          __routines_MOD_vmtopspspsdecay(&Px, &Py, &Pz, &E,
                                         &M, &M1, &M2,
                                         &Sqx, &Sqy, &Sqz,
                                         &kpx, &kpy,
                                         &P1x, &P1y, &P1z, &E1,
                                         &P2x, &P2y, &P2z, &E2,
                                         &P3x, &P3y, &P3z, &E3,
                                         &Dxx, &Dxy, &Dxz,
                                         &Dyx, &Dyy, &Dyz,
                                         &Dzx, &Dzy, &Dzz);
          // Fill daughters momenta.
          P1 = Vec4(P1x,P1y,P1z,E1);
          P2 = Vec4(P2x,P2y,P2z,E2);
          P3 = Vec4(P3x,P3y,P3z,E3);
          // Fill decay matrix.
          DecayMatrix[0][0] = Dxx;
          DecayMatrix[0][1] = Dxy;
          DecayMatrix[0][2] = Dxz;
          DecayMatrix[1][0] = Dyx;
          DecayMatrix[1][1] = Dyy;
          DecayMatrix[1][2] = Dyz;
          DecayMatrix[2][0] = Dzx;
          DecayMatrix[2][1] = Dzy;
          DecayMatrix[2][2] = Dzz;
          // Done.
          done = true;
      }
      Particle had[3];

      // Now set up final decay information for successfully handled decays.
      if(done){

        // Special treatmet for rho-, K*- and K0*bar as pickChannel
        // decays them as rho+, K*+ and K*0.
        int charge_1, charge_2;
        int id_in = decayer_in.id();
        int id_d1_in = channel.product(0);
        int id_d2_in = channel.product(1);

        // rho-
        if(id_in == -213){
          if(id_d1_in==211&&id_d2_in==111)
          {charge_1 = -1; charge_2 = 1;}
          else
          {charge_1 = 1; charge_2 = 1;}
        // K*-
        } else if(id_in==-323){
          if(id_d1_in==311 && id_d2_in==211)
            {charge_1 = -1; charge_2 = -1;}
          else if(id_d1_in==321 && id_d2_in==111)
            {charge_1 = -1; charge_2 =  1;}
          else
            {charge_1 = 1; charge_2 = 1;}
        // K*0bar
        } else if(id_in==-313){
          if(id_d1_in==321&&id_d2_in==-211)
            {charge_1 = -1; charge_2 = -1;}
          else if(id_d1_in==311&&id_d2_in==111)
            {charge_1 = -1; charge_2 = 1;}
          else
            {charge_1 = 1; charge_2 = 1;}
        }
        // For all others, let Pythia decide the decay products.
        else{
          charge_1 = 1; charge_2 = 1;
        }

        // Now construct particle objects with the daughters.
        had[0] = Particle(charge_1*channel.product(0), 0, 0, 0, 0, 0, 0, 0, P1, M1);
        had[1] = Particle(charge_2*channel.product(1), 0, 0, 0, 0, 0, 0, 0, P2, M2);

        // Add dauthers to the current decayer.
        decayer->setDaughter(had[0]);
        decayer->setDaughter(had[1]);
        decayer->decayed(true);

        // For three-body decays add also the third daughter.
        if(decayType==2){
          had[2] = Particle( channel.product(2), 0, 0, 0, 0, 0, 0, 0, P3, M3);
          decayer->setDaughter(had[2]);
        }
        // Add current decayer to the list of externally handled hadrons.
        savedHadrons.append(decayer,fromPos);
        return done;
      }

      // In principle the next lines should not be executed.
      decayer->decayed(false);
      savedHadrons.append(decayer,fromPos); 
      return false;
  }

  // Setup initial conditions for the string fragmenation.
  void init(const Vec4& pPosin, const Vec4& pNegin, const Vec4& SPosin,
            const RotBstMatrix& GNSin, const RotBstMatrix& posBoostin,
            const RotBstMatrix& negBoostin, const StringRotation& stringRotin,
            const bool negActive_in){
    pPos = pPosin;
    pNeg = pNegin;
    SPos = SPosin;
    GNS  = GNSin;
    posBoost = posBoostin;
    negBoost = negBoostin;
    stringRot = stringRotin;
    negActive = negActive_in;
  }

  // The method called by Pythia each time the decay of a handable hadron is to be simulated.
  virtual bool decay(vector<int>& idProd , vector<double>& mProd , vector<Vec4>& pProd ,
                     int iDec, const Event& event ) {

    // Do not simulate decays of hadrons not produced in string fragmentation.
    // They are handled by Pythia.
    if( !(event[iDec].statusAbs()==83 || event[iDec].statusAbs()==84) )
      return false;

    // Number of decayers handled externally.
    int size = savedHadrons.Decayers.size();

    // Find the internal index correspoding to the iDec given by Pythia.
    int index=-1;
    
    // The reconstruction of the internal index is different for hadrons
    // produced from the positive (status=83) and negative (status=84)
    // string ends.
    // Hadron from positive end.
    if(event[iDec].status()==83)
      for(int i=0; i<size; i++){
          int i_try = savedHadrons.iOffset()+savedHadrons.Decayers[i]->iR();
          if(i_try == iDec) index = i;
      }
    // Hadron from negative end.
    if(event[iDec].status()==84)
      for(int i=0; i<size; i++){
        int i_try = savedHadrons.iFrag()-savedHadrons.Decayers[i]->iL();
        if(i_try == iDec) index = i;
    }

    // If iDec is not reconstructed internally let Pythia decay the hadron.
    if(index==-1) {
      cout << "VectorMesonDecays::decay: Decayer index not correctly reconstructed.\n";
      cout << "VectorMesonDecays::decay: Letting Pythia simulate it.\n";
      return false;
    }

    // Now pass the decay products to Pythia, if the hadron has been decayed
    // externally. Otherwise let Pythia decay the hadron.
    if(savedHadrons.Decayers[index]->hasDecayed()){
      // The decays are handled in the GNS but Pythia stores the info in the
      // laboratory frame.
      RotBstMatrix GNS_back = GNS.inverse();
      RotBstMatrix endBoost;
      if(savedHadrons.Decayers[index]->isFromPos())
        endBoost = posBoost;
      else
        endBoost = negBoost;
      endBoost = endBoost.inverse();
      //Pass daughters to Pythia.
      for(int i=0;i<savedHadrons.Decayers[index]->Daughters.size(); i++){
        // Pass ids.
        idProd.push_back( savedHadrons.Decayers[index]->Daughters[i].id());
        // Pass masses.
        mProd.push_back(  savedHadrons.Decayers[index]->Daughters[i].m() );
        // Pass momenta, in the laboratory frame.
        Vec4 ph = savedHadrons.Decayers[index]->Daughters[i].p();
        stringRot.Rotate(ph);
        ph = endBoost*ph;
        stringRot.RotateBack(ph);
        ph = GNS_back*ph;
        pProd.push_back( ph );
       }
       return true;
    } else
      return false;

    return false;
  }

  // Event stored internally. Should not be needed.
  Event storedEvent;
  Vec4 SPos_copy;
  Vec4 SPos;

  private:

  // Vectors with identity codes of particles which can be
  // handled externally.
  vector<int> possibleDecays;

  // Initial conditions for string fragmentation.
  Vec4 pPos;
  Vec4 pNeg;

  // Matrix which brings from the laboratory frame to the GNS.
  RotBstMatrix GNS;

  // Matrices for the boost to the string rest frame.
  RotBstMatrix posBoost, negBoost;

  // True when the negative string end is active.
  bool negActive;

  // StringRotation object to handle primordial kT.
  StringRotation stringRot;

  // Pointer to the ParticleData class.
  ParticleData* particleData;

  // Stored id's of not handable products.
  int NH_prod1, NH_prod2, NH_prod3;

};

// End of VectorMesonDecays.h.
}
#endif

