#ifndef STRINGSPINNER_H
#define STRINGSPINNER_H

#include "Pythia8/Pythia.h"
#include <tuple>
#include <memory>
#include "Transversity.h"
#include "VectorMesonDecays.h"
#include "PrimordialKT.h"

#if PYTHIA_VERSION_INTEGER < 8310
#define ERROR(msg) infoPtr->errorMsg(msg)
#else
#define ERROR(msg) loggerPtr->ERROR_MSG(msg)
#endif
namespace Pythia8 {


/* Helper class for automatically restoring the values of some
   variables to their original values when a function exits. */
template <typename... Types>
class safekeep {

public:

  // Constructor taking a set of object that will be restored to the
  // original value when this object goes out of scope as long as
  // restoreOnDestruct is true
  safekeep(Types & ... args)
    : values(args...), variables(args...), restoreOnDestruct(true) {}

  // If restorOnDestruct is true, restore the valuess of the contained
  // variable to the value they had when this object was constructed.
  ~safekeep() {
    if ( restoreOnDestruct ) variables = values;
  }

  // Restore the values of the contained variables to the value they
  // had when this object was constructed.
  void restore() {
    variables = values;
    restoreOnDestruct = true;
  }

  // Discard the values the contained variables had when this object
  // was constructed.
  void discard() {
    restoreOnDestruct = false;
  }

  // Accept the current values of the contained variables as the one
  // to be restored on destruction.
  void accept() {
    values = variables;
    restoreOnDestruct = true;
  }

private:

  // Prevent copying of safekeep objects.
  safekeep & operator=(const safekeep &);

  // The list of values the variables had when this object was constructed.
  std::tuple<Types ...> values;

  // The list of references to the variables given when this object
  // was constructed.
  std::tuple<Types & ...> variables;

  // Flag to indicate that the variables should be restored.
  bool restoreOnDestruct;

};

// Convenience function to construct a safekeep object.
template <typename... Types>
safekeep<Types...>  make_safekeep(Types & ... args) {
  return safekeep<Types...>(args...);
}


    
// fortran routine for the calculation of q' polarization vector
extern "C" void __routines_MOD_outpol(double *KTPRIMX, double *KTPRIMY,
                                      double *SXX, double *SYY, double *SZZ,
                                      double *SOUTXX, double *SOUTYY, double *SOUTZZ);
// fortran routine for the calculation of q' polarization vector when a VM is emitted
extern "C" void __routines_MOD_outpol_vm(double *KTPRIMX, double *KTPRIMY,
                                         double *SXX, double *SYY, double *SZZ,
                                         double *SOUTXX, double *SOUTYY, double *SOUTZZ,
                                         double *DXX, double *DXY, double *DXZ,
                                         double *DYX, double *DYY, double *DYZ,
                                         double *DZX, double *DZY, double *DZZ);

// fortran routine for the calculation of the 3P0 weight
extern "C" void __routines_MOD_acceptpythia(double *KTPX, double *KTPY,
                                            double *SXX, double *SYY, int *flag,
                                            int *isVM, double *weight);

// fortran routine to set a default seed for the random number generator
extern "C" void __routines_MOD_setseed();

// fortran routine to setup the complex mass
extern "C" void __routines_MOD_setmu(double *re, double *im);

// fortran routine to setup the parameter GLGT
extern "C" void __routines_MOD_setglgt(double *glgt_in);

// fortran routine to setup the parameter ThetaLT
extern "C" void __routines_MOD_setthetalt(double *thetalt_in);

// fortran routine to setup the form factor in the 3-body decays
// of omega and phi.
extern "C" void __routines_MOD_setformfact(bool *formfact_in);

// This class encapsulates the standard DIS kinematics variables.
class DISKinematics {

public:

  // Empty constructor.
  DISKinematics() {}

  // Main constructor.
  DISKinematics(const Vec4 & lin, const Vec4 & lout, const Vec4 & hin)
    : lepin(lin), lepout(lout), hadin(hin), q(lin - lout) {
    Q2 = -q.m2Calc();
    W2 = (q + hin).m2Calc();
    //    xB = Q2/(2.0*hadin*q);
    xB = Q2/(Q2 + W2); // This prevents xB > 1
    y = (q*hin)/(lin*hin);
    //HCM = toCMframe(q, hin);
    HCM.toCMframe(q, hin);
    HCM.rot(0.0, -(HCM*lout).phi());
    GNS = HCM;
    GNS.bstback(HCM*hin);
    lplane = Vec4(lin.py()*lout.pz() - lin.pz()*lout.py(),
                  lin.pz()*lout.px() - lin.px()*lout.pz(),
                  lin.px()*lout.py() - lin.py()*lout.px(), 0.0);
  }

  // New: 17/03/2022
  Vec4 toMyGNS(Vec4 pin){
    Vec4 ptmp = pin;
    ptmp.rotaxis(q.theta(),lplane);
    Vec4 qtmp = lepout;
    qtmp.rotaxis(q.theta(),lplane);
    ptmp.rot(0.0,-qtmp.phi());
    //ptmp.rot(0.0,M_PI/2);
    return ptmp;
  }
  // The incoming lepton.
  Vec4 lepin = {};

  // The scattered lepton.
  Vec4 lepout = {};

  // The incoming hadron.
  Vec4 hadin = {};

  // The virtual photon.
  Vec4 q = {};

  // The Loretz transformation from the curent frame to the hadronic
  // rest frame (exchanged photon in the +z direction.
  RotBstMatrix HCM = {};

  // The rest frame of the hadron, longitudinally boosted from the HCM
  RotBstMatrix GNS = {};

  // The standard DIS variables.
  double Q2 = 0.0, W2 = 0.0, xB = 0.0, y = 0.0;

  // The lepton plane definition
  Vec4 lplane;

    
}; 

// Calculates the direction of the struck quark
// polarization vector.
inline Vec4 FindQuarkPol(Vec4& Snucl, double prob, Rndm * Rndm_Ptr){

  Vec4 Sqrk;
  if( Rndm_Ptr->flat() < 0.5 * ( 1.0 + prob ) )
    {Sqrk = Snucl;}
  else
    {Sqrk = -Snucl;}
	
  Sqrk.pz( 0.0 );
  Sqrk.e(  0.0 );

  return Sqrk;
}

// Reflect and depolarise according to QED.
inline Vec4 ReflectDepolarize(const Vec4& p, const double Y){

  Vec4 pp = p;
  double Dnn, Dll;
  Dnn = 2.0 * (1.0 - Y)/(1.0 + (1.0 - Y)*(1.0 - Y));
  Dll = -1.0;

  pp.px( -Dnn * p.px() );
  pp.py(  Dnn * p.py() );
  pp.pz(  Dll * p.pz() );
  pp.e( 0.0 );

  return pp;
}

/**
 * StringSpinnerHooks is a base class for UserHooks classes that can
 * reweight the string breakups in Pythia8 to emulate spin effects.
 */
class StringSpinnerHooks: public UserHooks,
                          public std::enable_shared_from_this<StringSpinnerHooks> {

public:

  // Member functions for output info.
  bool IsValenceQ() const {return ValenceHit;}
  int  PosEndId()	const {return idPosEnd;}
  int  NegEndId() 	const {return idNegEnd;}

  // the options for handling cases that string+3P0 model can't handle
  enum Mode {
    REJECT 	= -1,  /**	If Pythia suggests something we can't handle we
                                simply reject the suggestion. */
    IGNORE 	= 0,   /**	Pretend nothing happened and let the new string
                                end inherit the previous spin state. */
    DISABLE = 1   /**	Accept and continue without spin corrections. */
  };

  // mode types for external use
  Mode reject = REJECT;
  Mode ignore = IGNORE;
  Mode disable = DISABLE;
   //C MydecayHandler* mydecay();

  // Convenient typedef to save and restore values.
  typedef safekeep<Vec4,RotBstMatrix,Vec4,Vec4,RotBstMatrix,Vec4> Safe;

  
  // The empty constructor needed for automatic loading
  StringSpinnerHooks()
    : hadronMode(REJECT), gluonMode(DISABLE), remnantMode(REJECT),
      warnings(true), posActive(true), negActive(false), 
      eventPtr{}, OSR(2.0) {}

  // The constructor needs a Pythia object. The hook
  // will add itself to this Pythia object.
  StringSpinnerHooks(Pythia & pythia)
    : hadronMode(REJECT), gluonMode(DISABLE), remnantMode(REJECT),
      warnings(true), posActive(true), negActive(false), 
      eventPtr{}, OSR(2.0) {
    plugInto(pythia);
  }

  // Plug this object into Pythia as a user hook.
  void plugInto(Pythia & pythia) {
    eventPtr = &pythia.event;

    // These are the hadrons we are able to handle.
    possibleHadrons = { 111, 211, -211, 311, -311, 321, -321, 221, 331,
                        411, -411, 421, -421, 431, -431, 441,
                        521, -521, 511, -511, 531, -531, 541, -541, 551,
                        113, 213, -213, 313, -313, 323, -323, 223, 333,
                        413, -413, 423, -423, 433, -433, 443,
                        523, -523, 513, -513, 533, -533, 543, -543, 553 };

    // The set of hadrons which are deemed possible to be decayed.
    possibleDecays = {113, 213, -213, 313, -313, 323, -323, 223, 333};
    vector<int> decayMesons(possibleDecays.begin(), possibleDecays.end());

    // Inform Pythia about the decays we want to handle externally.
    extDecayPtr = std::make_shared<PolarizedDecayHandler>();
    extDecayPtr->plugInto(pythia, decayMesons);

    // Inform Pythia about which settings we want to make available.
    settingsPtr = &pythia.settings;
    Settings & settings = pythia.settings;
    settings.addMode("StringSpinner:gluonMode", 1, true, true, -1, 1);
    settings.addMode("StringSpinner:hadronMode", -1, true, true, -1, 1);
    settings.addMode("StringSpinner:remnantMode", -1, true, true, -1, 1);
    settings.addParm("StringSpinner:Re(mu)", 0.42, true, false, 0.0, 0.0); 
    settings.addParm("StringSpinner:Im(mu)", 0.76, false, false, 0.0, 0.0); 
    settings.addParm("StringSpinner:GLGT", 1.0, true, false, 0.0, 0.0); 
    settings.addParm("StringSpinner:thetaLT", 0.0, true, true, -M_PI, M_PI);
    settings.addFlag("StringSpinner:useOmegaPhiTo3PiFormFactor", false);
    settings.addPVec("StringSpinner:targetPolarisation", vector<double>(),
                     true, true, -1.0, 1.0);
    settings.addPVec("StringSpinner:dPolarisation", vector<double>(),
                     true, true, -1.0, 1.0);
    settings.addPVec("StringSpinner:uPolarisation", vector<double>(),
                     true, true, -1.0, 1.0);
    settings.addPVec("StringSpinner:sPolarisation", vector<double>(),
                     true, true, -1.0, 1.0);
    settings.addPVec("StringSpinner:cPolarisation", vector<double>(),
                     true, true, -1.0, 1.0);
    settings.addPVec("StringSpinner:bPolarisation", vector<double>(),
                     true, true, -1.0, 1.0);
    settings.addPVec("StringSpinner:dbarPolarisation", vector<double>(),
                     true, true, -1.0, 1.0);
    settings.addPVec("StringSpinner:ubarPolarisation", vector<double>(),
                     true, true, -1.0, 1.0);
    settings.addPVec("StringSpinner:sbarPolarisation", vector<double>(),
                     true, true, -1.0, 1.0);
    settings.addPVec("StringSpinner:cbarPolarisation", vector<double>(),
                     true, true, -1.0, 1.0);
    settings.addPVec("StringSpinner:bbarPolarisation", vector<double>(),
                     true, true, -1.0, 1.0);
    pythia.addUserHooksPtr(shared_from_this());
  }

  // Helper function for setting polarisation Vectors.
  bool setPol(Vec4 & pol, const vector<double> & v) {
    if ( v.empty() ) return false;
    if ( v.size() > 0 ) pol.px(v[0]);
    if ( v.size() > 1 ) pol.py(v[1]);
    if ( v.size() > 2 ) pol.pz(v[2]);
    return true;
  }

  // Initialisation after beams have been set by Pythia::init().
  virtual bool initAfterBeams() {
    __routines_MOD_setseed();
    gluonMode = Mode(settingsPtr->mode("StringSpinner:gluonMode"));
    remnantMode = Mode(settingsPtr->mode("StringSpinner:remnantMode"));
    hadronMode = Mode(settingsPtr->mode("StringSpinner:hadronMode"));
    double reMu = settingsPtr->parm("StringSpinner:Re(mu)");
    double imMu = settingsPtr->parm("StringSpinner:Im(mu)");
    __routines_MOD_setmu(&reMu,&imMu);
    double GLGT = settingsPtr->parm("StringSpinner:GLGT");
    __routines_MOD_setglgt(&GLGT);
    double thetaLT = settingsPtr->parm("StringSpinner:thetaLT");
    __routines_MOD_setthetalt(&thetaLT);
    bool ff = settingsPtr->flag("StringSpinner:useOmegaPhiTo3PiFormFactor");
    __routines_MOD_setformfact(&ff);
    Vec4 dpol, upol, spol, cpol, bpol,
      dbarpol, ubarpol, sbarpol, cbarpol, bbarpol;
    if ( setPol(SprotonLAB, settingsPtr->pvec("StringSpinner:targetPolarisation")) )
      AsymmOrApow = 1;
    if ( setPol(dpol, settingsPtr->pvec("StringSpinner:dPolarisation")) )
      qS[1] = dpol;
    if ( setPol(upol, settingsPtr->pvec("StringSpinner:uPolarisation")) )
      qS[2] = upol;
    if ( setPol(spol, settingsPtr->pvec("StringSpinner:sPolarisation")) )
      qS[3] = spol;
    if ( setPol(cpol, settingsPtr->pvec("StringSpinner:cPolarisation")) )
      qS[4] = cpol;
    if ( setPol(bpol, settingsPtr->pvec("StringSpinner:bPolarisation")) )
      qS[5] = bpol;
    if ( setPol(dbarpol, settingsPtr->pvec("StringSpinner:dbarPolarisation")) )
      qS[-1] = dbarpol;
    if ( setPol(ubarpol, settingsPtr->pvec("StringSpinner:ubarPolarisation")) )
      qS[-2] = ubarpol;
    if ( setPol(sbarpol, settingsPtr->pvec("StringSpinner:sbarPolarisation")) )
      qS[-3] = sbarpol;
    if ( setPol(cbarpol, settingsPtr->pvec("StringSpinner:cbarPolarisation")) )
      qS[-4] = cbarpol;
    if ( setPol(bbarpol, settingsPtr->pvec("StringSpinner:bbarPolarisation")) )
      qS[-5] = bbarpol;
    if ( ! qS.empty() ) AsymmOrApow = 2;
    
    
    return true;
  }

  // Function to recognize a vector meson;
  bool isVectorMeson(int id_in){
    bool isVM = (((abs(id_in)%100)%10 == 3))? true:false;
      //if(id_in == 113) cout << "isVM" << endl;
    return isVM;
  }
    
  // Function to check if a hadron will be decayed externally.
  bool willDecayExternally(Particle& p){
    if( p.isFinal()
        && possibleDecay(p.id()) && isVectorMeson(p.id())
        && (p.status()==83 || p.status()==84) )
      return true;
    return false;
  }
    
  // Check if spin modelling is possible for the given particle type.
  bool possible(int id) const {
    return possibleHadrons.find(id) != possibleHadrons.end();
  }

  // The hook function to tell Pythia that we want to interfere with
  // the string fragmentation.
  virtual bool canChangeFragPar() {
    return true;
  }

  // The function called from Pythia each time a new string (piece)
  // is set up for hadronisation.
  virtual void setStringEnds(const StringEnd * posEnd,
                             const StringEnd * negEnd,
                             vector<int> iParton) {

    Event & event = *eventPtr;
    // Save the indices of the partons in the string (piece).
    idx = iParton;
		
    // If this is a junction system, treat it as if there were gluons
    // on the strings.
    if ( !negEnd ) {
      if ( gluonMode == IGNORE ) {
        posActive = negActive = false;
        if ( warnings )
          ERROR("StringSpinner warning: Cannot handle junctions. "
                "Spin not treated in this string.");
        return;
      } else
        negActive = false;
    }

    // If this is not a simple q-qbar string switch off spin effects
    // for both sides.
    if ( idx.size() != 2 && gluonMode == IGNORE ) {
      posActive = negActive = false;
      if ( warnings )
        ERROR("StringSpinner warning: Cannot handle gluons. "
              "Spin not treated in this string.");
      return;
    }

    // Momenta of partons in the string.
    posMom = event[idx[0]].p();
    negMom = event[idx.back()].p();

    // Id's of string endpoints.
    idPosEnd = event[idx[0]].id();
    idNegEnd = event[idx.back()].id();

    // Set up the kinematic variables.
    dis = DISKinematics(event[1].p(), event[5].p(), event[2].p());
		
    // Temporary momenta of struck quark and target remnant,
    // in the GNS reference system.
    Vec4 pStruckQuark = dis.GNS * posMom; 
    Vec4 pRemnant     = dis.GNS * negMom;
    
    // Construct boost matrices to the string rest frame.
    posBoost = negBoost = RotBstMatrix();

    // Bring the plane formed by the string ends to the lepton scattering plane.
    Vec4 lplane = dis.lplane;
    lplane.rotbst(dis.GNS);
    bool SeaAntiQuarkHit = false; // temporary
    stringRot = StringRotation(pStruckQuark, pRemnant, lplane, SeaAntiQuarkHit);
    stringRot.Rotate(pStruckQuark);
    stringRot.Rotate(pRemnant);
    
    // Boost the string ends to the string rest frame.
    posMom = pStruckQuark;
    negMom = pRemnant;
    posBoost.toCMframe(posMom,negMom);
    negBoost.toCMframe(negMom,posMom);

    // Update momenta of string ends (now in the string rest frame).
    posMom.rotbst(posBoost);
    negMom.rotbst(negBoost);

    // Polarization vector for the string endpoints.
    posS = negS = Vec4();
			
    // Activate spin effects.
    if( infoPtr->isValence2() ) {
      posActive = true;
      setupStringSpin(posEnd, negEnd);
      ValenceHit = true;
    } else {
      posActive = false;
      ValenceHit = false;
    }
    negActive = false;
      
    // Input for the external decay routine: initial conditions of the string fragmentation.
    extDecayPtr->storedEvent = event;
    extDecayPtr->init(posMom, negMom, posS, dis.GNS, posBoost, negBoost, stringRot, negActive);
    extDecayPtr->savedHadrons = SavedHadrons(event.size(), idPosEnd, idNegEnd,
                                               posMom, negMom);
  }

  // Setup for the polarization vectors of string ends.
  virtual void setupStringSpin(const StringEnd * ,
                               const StringEnd * ) {}

  // Function to call the external decay.
  virtual void Decay(Particle p_in, Vec4& Sq_in, Vec4& kTprim_in,
                     double DecayMatrix_in[3][3], RotBstMatrix& GNS_in, bool fromPos_in){
    // If the decay of the hadron can be handled externally decay it.
    // Otherwise keep track of the hadron internally.
    if( possibleDecay(p_in.id()) )
      extDecayPtr->doPolarizedDecay(p_in, Sq_in, kTprim_in, DecayMatrix_in, GNS_in, fromPos_in);
    else
      extDecayPtr->savedHadrons.increment(fromPos_in);
    // Save from which side the last hadron has been produced.
    lastEnd = (fromPos_in)? 83:84; 
    // Save the type of the produced hadron.
    if(fromPos_in)
      lastHadRight = p_in.id();
    else
      lastHadLeft = p_in.id();
  }

  // The function called from Pythia each time a new hadron is
  // chopped off from a string piece.
  virtual bool doVetoFragmentation( Particle p, const StringEnd * end) {
    
    // Needed for unpolarized decays.
    Vec4 kTprim0 = Vec4(); 
    Vec4 SqUnpol = Vec4();
    double DecayMatrixUnpol[3][3]={0.0};
    
    // Trial hadrons with negative energy produce errors in external decay
    // routines. Reject them.
    if(p.e()<0) return true;

    // Decide what to do when both string endpoints are unpolarized.
    // Each time a false has to be returned, the accepted hadron has to
    // be decayed externally (if it is the case) or kept track internally.
    // This is done by the Decay function.
    if (!posActive && !negActive) {
      if (hadronMode==DISABLE){
          Decay(p, SqUnpol, kTprim0, DecayMatrixUnpol, dis.GNS, end->fromPos);
          return false;
      }
      else if (hadronMode==REJECT) {
        if(!possible(p.id())) return true;
        else if(rndmPtr->flat()< 1./OSR) return true;
        else {
          Decay(p, SqUnpol, kTprim0, DecayMatrixUnpol, dis.GNS, end->fromPos);
          return false;
        }
      }
      else {
        Decay(p, SqUnpol, kTprim0, DecayMatrixUnpol, dis.GNS, end->fromPos);
        return false;
      }
    }

    // Safe will automatically reset the values if not discard() has
    // been called.
    Safe safe(posMom, posBoost, posS, negMom, negBoost, negS);

    // If a proposed splitting cannot be handled, it is
    // rejected with a factor OSR (0.5) to conserve the normalization
    // of the splitting function.
    double weight = 1.0/OSR;

    // Make sure the produced hadron can be handled and decide wether
    // to veto it.
    if ( possible(p.id()) ) {
      if ( end->fromPos ) {
        weight = getSpinWeight(end, posMom, posBoost, posS, p);
        // posMom is now the four momentum of q'.
      }
      else {
        if( remnantMode == REJECT ) return true;
        else {
          Decay(p, SqUnpol, kTprim0, DecayMatrixUnpol, dis.GNS, end->fromPos);
          return false;
        }
      }
    } else {
      if ( hadronMode == REJECT ) return true;
      if ( hadronMode == DISABLE ) { // Shouldn't here return false?
        ( end->fromPos? posActive: negActive ) = false;
      }
    }
    // Now decide what to do with the hadron that can be treated.
    // Accept it with probability given by "weight" and simulate the polarized decay.
    // Then propagate quark polarization.
    if ( weight > rndmPtr->flat() ) {
      // Decay the accepted hadron.
      Decay(p, posS, posMom, DecayMatrix, dis.GNS, end->fromPos);
      // Now propagate quark polarization.
      propagateSpin(posMom,posS,p,DecayMatrix);

      safe.discard();
      return false;
    }

    return true;

  }

  // Last hadrons produced in the joining procedure after the creation of
  // the final two partons.
  virtual bool doVetoFragmentation(Particle p1, Particle p2,
                                   const StringEnd* end1, const StringEnd* end2){

    // Hadron from positive end (always p1) is decayed polarized
    // if the polarization is being propagated from the positive end.
    Vec4 phad1 = dis.GNS*p1.p();
    stringRot.Rotate(phad1);
    phad1 = posBoost*phad1;
    stringRot.RotateBack(phad1);
    Vec4 kTprim = posMom - phad1;
    Vec4 Squark = Vec4(posS.px(), posS.py(), posS.pz(), 0.0);
    if(posActive)
      Decay(p1, Squark, kTprim, DecayMatrix, dis.GNS, end1->fromPos);
    else{
      kTprim = Vec4();
      Squark = Vec4();
      Decay(p1, Squark, kTprim, DecayMatrix, dis.GNS, end1->fromPos);
    }

    // Hadron from negative end is decayed unpolarized.
    kTprim = Vec4();
    Squark = Vec4();
    double DecayMatrixUnpol[3][3];
    Decay(p2, Squark, kTprim, DecayMatrixUnpol, dis.GNS, end2->fromPos);

    // Do not reject the final two hadrons.
    return false;
  }

  // Calculate the relative spin weight for the produced hadron
  virtual double getSpinWeight(const StringEnd *,
                               Vec4 &, RotBstMatrix &, Vec4 &,
                               const Particle & ) {
    return 1.0/OSR;
  }

  // Propagates the quark spin according to the string+3P0 model.
  virtual void propagateSpin(Vec4&, Vec4 &, const Particle &,
                             double D[3][3] ) {
  // Implementation in SimpleStringSpinner.
  }
    

protected:

  // Add a particle id to the list of hadrons that can be handled
  void addPossibleHadron(int id) {
    possibleHadrons.insert(id);
  }

  // Add particle ids to the list of hadrons that can be handled
  void addPossibleHadrons(string ids) {
    istringstream is(ids);
    int id;
    while ( is >> id ) possibleHadrons.insert(id);
  }
    
  // Add a particle id to the list of hadrons whose decays can be handled externally.
  void addPossibleDecay(int id) {
    possibleDecays.insert(id);
  }

  // Check if we want to handle the decay of this particle externally.
  bool possibleDecay(int id) const {
    return possibleDecays.find(id) != possibleDecays.end();
  }

protected:

  // The set of hadrons which are deemed possible to handle. 
  set<int> possibleHadrons;
  set<int> possibleDecays;

  // Switches to determine the strategy for handling hadrons that are
  // not possible to handle, gluons and splittings from the target remnant.
  int hadronMode;
  int gluonMode;
  int remnantMode;

  // Switch to tell if warnings are emitted when the spin treatment is
  // disabled for a string.
  bool warnings;

  // Flags indicating if spin correction are active for the positive
  // and negative string ends.
  bool posActive;
  bool negActive;

  // Current momenta of the positive and negative string ends.
  Vec4 posMom;
  Vec4 negMom;

  // Id's of string endpoints.
  int idPosEnd;
  int idNegEnd;

  // Current polarisation vectors of string ends.
  Vec4 posS;
  Vec4 negS;
		
  // Flag for a valence quark hit.
  bool ValenceHit;

  // Keep track of DIS kinematics if necessary.
  DISKinematics dis;
  
  // Class to handle rotations due to primordial kT.
  StringRotation stringRot;

  // Switch to choose between asymmetry or analysing power.
  int AsymmOrApow;

  // Polarization vector of the target in LAB system.	
  Vec4 SprotonLAB;

  // Quark polarization vectors for analysing power.
  map<int,Vec4> qS;
  
  // Boost to the frame where the spin
  // of the positive/negative string end is given.
  RotBstMatrix posBoost;
  RotBstMatrix negBoost;

  // The indeices of the partons in the current string (piece).
  vector<int> idx;
  
  // The Pythia object to which this hook is assigned.
  //  Pythia & pythia;

  // The Event of the Pythia object to which this hook is assigned.
  Event * eventPtr;

  // The oversampling rate. For every sampling of the fragmentation
  // function, we allow OSR tries, and on average throw away with a
  // factor 1/OSR. Normally OSR needs to be at least 2.
  double OSR;
    
  // Class to handle the external decays.
  std::shared_ptr<PolarizedDecayHandler> extDecayPtr;


  // String end from where the last hadron has been produced.
  int lastEnd = 0; 

  // Type of the last hadron produced from each string end.
  int lastHadRight = 0;
  int lastHadLeft = 0;

  // Running decay matrix from the positive end.
  double DecayMatrix[3][3];
};


// Implementation of a StringSpinnerHooks class.
class SimpleStringSpinner: public StringSpinnerHooks {

public:
  
  // Member functions for output info.
  double 	xh1q()		const {return xh1_save;		}
  double	xf1q() 		const {return xxf1;		}
  Vec4	SQuarkGNS()	const {return Squark_save;	}
  Vec4	STargetGNS()	const {return SprotonGNS_save;	}
  Vec4	SFragQuarkGNS()	const {return posS_save;	}

  // Constructor.
  SimpleStringSpinner(): StringSpinnerHooks() {}
  SimpleStringSpinner(Pythia & pytin): StringSpinnerHooks(pytin) {}

  // Polarization vector of the string endpoints.
  virtual void setupStringSpin(const StringEnd * , const StringEnd *) {

    Event & event = *eventPtr;
    posS = Vec4(0.0, 0.0, 0.0, 0.0);
    negS = Vec4(0.0, 0.0, 0.0, 0.0);

    // Unpolarized PDF for the struck quark.
    xxf1 = beamBPtr->xfVal((*beamBPtr)[0].id(), dis.xB, dis.Q2);

    // If the target polarisation has been specified
    // polarise the struck quark.
    if( AsymmOrApow == 1)  {

      // Rotate target polarization from lab to gns.
      SprotonGNS = dis.GNS*SprotonLAB;	
      
      // Transversity PDFs for valence u and d quarks.
      xh1uP = uTransvPDF( dis.xB , dis.Q2 );	
      xh1dP = dTransvPDF( dis.xB , dis.Q2 );
      xh1uN = xh1dP;
      xh1dN = xh1uP;
      // Use isospin asymmetry for a neutron target.
      if( event[2].id() == 2212 ) {
        xh1u = xh1uP;
        xh1d = xh1dP;
      } else if ( event[2].id() == 2112 ) {
        xh1u = xh1uN;
        xh1d = xh1dN;
      } else {
        xh1u = 0.0;
        xh1d = 0.0;
      }

      // Polarise the struck quark.
      if(event[idx[0]].id() == 2 ){
        puv = xh1u / xxf1;
        Squark = FindQuarkPol(SprotonGNS, puv, rndmPtr);
        posS = ReflectDepolarize(Squark,dis.y);
        xh1_save = xh1u;
        Squark_save = Squark;
      } else if (event[idx[0]].id() == 1) {		
        pdv = xh1d / xxf1;
        Squark = FindQuarkPol(SprotonGNS, pdv, rndmPtr);	
        posS = ReflectDepolarize(Squark,dis.y);
        xh1_save = xh1d;
        Squark_save = Squark;
      } else {
        posS = Vec4(0.0, 0.0, 0.0, 0.0);
        xh1_save = 0.0;
        Squark_save = Vec4();
      }

      // If the quark polarisation has been specified.	
    } else if ( AsymmOrApow == 2 ) {	
      Vec4 temp_posS = qS[event[idx[0]].id()];
      temp_posS = dis.GNS*temp_posS;
      posS = ReflectDepolarize(temp_posS,0.0);
      Squark_save = Squark;
      posS_save = posS;
      xh1_save = 0.0;
    }
      extDecayPtr->SPos_copy = posS;
      extDecayPtr->SPos = posS;
  }

  // Calculation of the 3P0 weight.
  virtual double getSpinWeight(const StringEnd *,
                               Vec4& pend, RotBstMatrix & Rend, Vec4 & S,
                               const Particle & p) {

    //Boost current hadron to GNS, then to the string rest frame.
    Vec4 phad = dis.GNS*p.p();
    stringRot.Rotate(phad);
    phad = Rend*phad;
    stringRot.RotateBack(phad);

    // Transverse momentum of current string end (quark q).
    double KTX = pend.px();
    double KTY = pend.py();

    // Polarization of current string end.
    double SINX = S.px();
    double SINY = S.py();
    double SINZ = S.pz();

    // Update the four momentum of the new string end (quark q').
    pend -= phad;
    double KTPRIMX = pend.px();
    double KTPRIMY = pend.py();

    // Calculate the spin-dependent weight associated to the production of the current hadron.
    double weight = 0.0;
    int FLAG = 0;
    int isVM = (((abs(p.id())%100)%10 == 3))? 1:0;
    // Call the Fortran routine.
    __routines_MOD_acceptpythia(&KTPRIMX,&KTPRIMY,&SINX,&SINY,&FLAG,&isVM,&weight);
    if(weight/OSR<0 || weight/OSR>1)
        cout << "StringSpinner: the weight factor is larger than 1: " << weight/OSR << endl;

    return weight/OSR;
  }

  // Propagation of quark polarization according to the string+3P0 model.
  virtual void propagateSpin(Vec4& pend, Vec4& S, const Particle & p,
                             double D[3][3]) {
    // Here pend is already updated to the quark q'.
    double KTPRIMX = pend.px();
    double KTPRIMY = pend.py();

    // Polarization vector of the quark q.
    double SINX = S.px();
    double SINY = S.py();
    double SINZ = S.pz();
    
    // Calculate the polarization vector of the quark q'.
    double SOUTX=0.,SOUTY=0.,SOUTZ=0.;
    int isVM = (((abs(p.id())%100)%10 == 3))? 1:0;
    // The rules are different depending on the emitted hadron spin.
    if(isVM==1)
      __routines_MOD_outpol_vm(&KTPRIMX,&KTPRIMY,&SINX,&SINY,&SINZ,&SOUTX,&SOUTY,&SOUTZ,
                               &D[0][0],&D[0][1],&D[0][2],
                               &D[1][0],&D[1][1],&D[1][2],
                               &D[2][0],&D[2][1],&D[2][2]);
    else
      __routines_MOD_outpol(&KTPRIMX,&KTPRIMY,&SINX,&SINY,&SINZ,&SOUTX,&SOUTY,&SOUTZ);

    // Update the string end polarisation vector to that of q'.
    S.px(SOUTX);
    S.py(SOUTY);
    S.pz(SOUTZ);

  }  

protected:
  // Values of transversity PDFs.
  double 	xh1u, xh1d;
  double 	xh1uP, xh1uN, xh1dP, xh1dN;
  double	xh1_save = 0.0;
  double	puv, pdv;
  // Values of unpolarised PDF.
  double 	xxf1;
  // Struck quark polarisation.
  Vec4 Squark, Squark_save;
  Vec4 posS_save;
  // Target polarisation in gns.
  Vec4 SprotonGNS, SprotonGNS_save;

};


}

#endif
