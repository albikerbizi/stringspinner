! This module gathers the definitions of new types
! of variables such as four-vectors and density matrices,
! to be used by the Fortran routines of StringSpinner.

module NewVariables
implicit none

! Kind for real numbers, 8 is needed to interface
! with Pythia (corresponding to double in c++).
integer, parameter :: rk=selected_real_kind(8)

private
! Public methods involving four-vectors and density matrices.
public Vec4
public VecInit
public InvMass
public InvMass2
public PAbs
public PTAbs
public PTAbs2
public PhiAng
public ThetaAng
public Rap
public PPlus
public PMinus
public Scalar3
public Scalar4
public Vector3
public Rot
public Bst
public BstToRestFrame
public nullSDM
public initSDM
public initSDMElem
public SpinDensityMatrix

! Definition of operations with four-vectors.
public assignment(=)
public operator(*)
public operator(/)
public operator(+)
public operator(-)

! Multiplication by scalar and Lorentz invariant
! scalar product.
interface operator(*)
	module procedure Vec4Mult
	module procedure Vec4Multex
	module procedure Vec4Prod
end interface

! Division by a scalar.
interface operator(/)
	module procedure Vec4Div
end interface

! Addition of two four-vectors.
interface operator(+)
	module procedure Vec4Add
end interface

! Difference of two four-vectors.
interface operator(-)
	module procedure Vec4Diff
end interface

! Equating of two four vectors.
interface assignment(=)
	module procedure Vec4Eq
end interface

!Definition of the four-vector class.
type Vec4
	real(kind=rk)	:: Px
	real(kind=rk)	:: Py
	real(kind=rk)	:: Pz
	real(kind=rk)	:: E

	contains
	procedure	:: PAbs
	procedure	:: PTAbs
	procedure	:: PTAbs2
	procedure	:: InvMass
	procedure	:: InvMass2
	procedure	:: PhiAng
	procedure	:: ThetaAng
	procedure	:: Rap
	procedure	:: PPlus
	procedure	:: PMinus
	procedure	:: Scalar3
	procedure	:: Scalar4
	procedure	:: Vector3
	procedure	:: Rot
	procedure	:: Bst
end type

! Definition of a spin density matrix,
! thought for vector mesons. It is defined
! in the rest frame of the particle.
type SpinDensityMatrix
    real(kind=rk) :: xx, xy, xz
    real(kind=rk) :: yx, yy, yz
    real(kind=rk) :: zx, zy, zz

    contains
    procedure   :: init => initSDM
    procedure   :: null => nullSDM
    procedure   :: initElements => initSDMElem
    procedure   :: print => printSDM
end type

contains
!*********************************************************!
!       Definitoons of operations with four-vectors       !
!*********************************************************!

! Multiplication by a scalar from right.
pure function Vec4Mult(vec,xx) result(vecOut)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk), intent(in) :: xx
	type(Vec4) :: vecOut

	vecOut%E  = xx * vec%E
	vecOut%Px = xx * vec%Px
	vecOut%Py = xx * vec%Py
	vecOut%Pz = xx * vec%Pz
end function

! Multiplication by a scalar from left.
pure function Vec4Multex(xx,vec) result(vecOut)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk), intent(in) :: xx
	type(Vec4) :: vecOut

	vecOut%E  = xx * vec%E
	vecOut%Px = xx * vec%Px
	vecOut%Py = xx * vec%Py
	vecOut%Pz = xx * vec%Pz
end function

! Product of two four vectors, gives the invariant
! scalar product.
pure function Vec4Prod(v1,v2) result(xx)
	implicit none
	class(Vec4), intent(in) :: v1, v2
	real(kind=rk) :: xx

	xx  = v1%E*v2%E - v1%Px*v2%Px&
	    - v1%Py*v2%Py - v1%Pz*v2%Pz
end function

! Division by a scalar.
pure function Vec4Div(vec,xx) result(vecOut)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk), intent(in) :: xx
	type(Vec4) :: vecOut

	vecOut%E  = vec%E / xx
	vecOut%Px = vec%Px / xx
	vecOut%Py = vec%Py / xx
	vecOut%Pz = vec%Pz / xx
end function

! Addition between two 4-vectors.
pure function Vec4Add(v1,v2) result(v3)
	implicit none
	class(Vec4), intent(in) :: v1,v2
	type(Vec4) :: v3

	v3%E  = v1%E  + v2%E
	v3%Px = v1%Px + v2%Px
	v3%Py = v1%Py + v2%Py
	v3%Pz = v1%Pz + v2%Pz
end function

! Subtraction between two 4-vectors.
pure function Vec4Diff(v1,v2) result(v3)
	implicit none
	class(Vec4), intent(in) :: v1,v2
	type(Vec4) :: v3

	v3%E  = v1%E  - v2%E
	v3%Px = v1%Px - v2%Px
	v3%Py = v1%Py - v2%Py
	v3%Pz = v1%Pz - v2%Pz
end function

! Equal operation of two four-vectors.
pure subroutine Vec4Eq(v1,v2)
	implicit none
	class(Vec4), intent(inout) :: v1
	class(Vec4), intent(in)	   :: v2

	v1%E  = v2%E
	v1%Px = v2%Px
	v1%Py = v2%Py
	v1%Pz = v2%Pz
end subroutine

! Initialises a 4-vector to the components
! given in parenthesis.
function VecInit(ee,xx,yy,zz) result (vec)
	implicit none
	real(kind=rk), intent(in) :: ee, xx, yy, zz
	type(Vec4) :: vec
	
	vec%E = ee
	vec%Px = xx
	vec%Py = yy
	vec%Pz = zz
end function

! Gives the magnitude of the spatial part of a 4-vector.
function PAbs(vec) result (pp)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk)	:: pp

	pp = vec%Px**2 + vec%Py**2 + vec%Pz**2
	pp = sqrt( pp )
end function

! Gvives the magnitude of the transverse component
! of a 4-vector.
function PTAbs(vec) result (ptt)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk) :: ptt

	ptt = sqrt(vec%Px**2+vec%Py**2)
end function

! Gives the magnitude squared of the transverse component
! of a 4-vector.
function PTAbs2(vec) result (ptt2)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk) :: ptt2

	ptt2 = vec%Px**2+vec%Py**2
end function

! Gives the azimuthal angle of a 4-vector between -pi and pi.
function PhiAng(vec) result (xx)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk) :: xx

	xx = atan2(vec%Py,vec%Px)
end function

! Gives the polar angle of a 4-vector between 0 and pi.
function ThetaAng(vec) result (xx)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk) :: xx
	
	xx = acos(vec%Pz/PAbs(vec))
end function

! Gives the invatiant mass of a 4-vector.
function InvMass(vec) result(invM)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk)	:: invM

    invM = sqrt(vec%E**2-vec%Px**2-vec%Py**2-vec%Pz**2)
end function

! Gives the invariant mass squared of a 4-vector.
function InvMass2(vec) result(invM2)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk)	:: invM2

    invM2 = vec%E**2-vec%Px**2-vec%Py**2-vec%Pz**2
end function

! Gives the positive lightcone momentum v^+=v^0+v^3
! of a four vector v.
function PPlus(vec) result(xx)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk)	:: xx

	xx = vec%E + vec%Pz
end function

! Gives the negative lightcone momentum v^-=v^0-v^3
! of a four vector v.
function PMinus(vec) result(xx)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk)	:: xx

	xx = vec%E - vec%Pz
end function

! Gives the rapidity of a 4-vector along the z axis.
function Rap(vec) result(xx)
	implicit none
	class(Vec4), intent(in) :: vec
	real(kind=rk)	:: xx

	xx = 0.5 * log(Pplus(vec)/PMinus(vec))
end function

! Scalar product between two 4-vectors v1 and v2.
! Gives the euclidean scalar product v1.v2.
function Scalar3(v1,v2) result(xx)
	implicit none
	class(Vec4), intent(in) :: v1, v2
	real(kind=rk) :: xx

	xx = v1%Px*v2%Px + v1%Py*v2%Py + v1%Pz*v2%Pz
end function

! 4-dimensional scalar product with signature (1,-1,-1,-1).
function Scalar4(v1,v2) result(xx)
	implicit none
	class(Vec4), intent(in) :: v1, v2
	real(kind=rk) :: xx

	xx = v1%Px*v2%Px + v1%Py*v2%Py + v1%Pz*v2%Pz
	xx = v1%E*v2%E - xx
end function

! Vector product v3 = v1 x v2, where the zero component
! of v3 il set to zero.
function Vector3(v1,v2) result(v3)
	implicit none
	class(Vec4), intent(in) :: v1, v2
	type(Vec4) :: v3

	v3%E  = 0.0
	v3%Px = v1%Py*v2%Pz-v1%Pz*v2%Py
  	v3%Py = v1%Pz*v2%Px-v1%Px*v2%Pz
  	v3%Pz = v1%Px*v2%Py-v1%Py*v2%Px
end function

! Rotation of a four-vector about y of theta and about
! z of phi.
function Rot(vec,theta,phi) result (vecOut)
	implicit none
	class(Vec4), intent(in) ::  vec
	real(kind=rk), intent(in) :: theta, phi
	type(Vec4)	:: vecOut
	type(Vec4)	:: vecTemp

	!rotate about y of an angle theta
	vecTemp%Px = cos(theta) * vec%Px + sin(theta) * vec%Pz 
	vecTemp%Py = vec%Py
	vecTemp%Pz = -sin(theta) * vec%Px + cos(theta) * vec%Pz

	!rotate about z of an angle phi
	vecOut%Px = cos(phi) * vecTemp%Px - sin(phi) * vecTemp%Py 
	vecOut%Py = sin(phi) * vecTemp%Px + cos(phi) * vecTemp%Py
	vecOut%Pz = vecTemp%Pz
	vecOut%E = vec%E
end function

! Lorentz boost with velocity (bx,by,bz) of a four-vector "vec".
! If the velocity is p/E brings to the rest frame of the particle
! with mass m**2 = E**2 - p**2.
function Bst(vec,bx,by,bz) result (vecOut)
	implicit none
	class(Vec4), intent(in) ::  vec
	real(kind=rk), intent(in) :: bx, by, bz
	type(Vec4)	:: vecOut
	type(Vec4)	:: beta, betau, vec0
	real(kind=rk)	:: gammaL
	
	beta = VecInit(0.0_rk,bx,by,bz)
	betau = beta / sqrt( Scalar3(beta,beta) )
	gammaL = 1.0 / sqrt(1.0 - Scalar3(beta,beta))
	vec0 = vec

	vecOut%E = gammaL * (vec0%E - Scalar3(beta,vec0))
	vecOut%Px = vec0%Px + (gammaL-1.0)*Scalar3(betau,vec0)*betau%Px&
		  - gammaL * beta%Px * vec0%E

	vecOut%Py = vec0%Py + (gammaL-1.0)*Scalar3(betau,vec0)*betau%Py&
		  - gammaL * beta%Py * vec0%E

	vecOut%Pz = vec0%Pz + (gammaL-1.0)*Scalar3(betau,vec0)*betau%Pz&
		  - gammaL * beta%Pz * vec0%E
end function


! Boost of vec1 to the rest frame of vec2: first a transverse boost,
! then a longitudinal boost.
function BstToRestFrame(vec1, vec2) result (vecOut)
	implicit none
	class(Vec4), intent(in) ::  vec1, vec2
	type(Vec4)	:: vecOut
	type(Vec4)	:: vecL1, vecL2
	
	vecL1 	= Bst(vec1,0.0_rk,0.0_rk,vec2%Pz/vec2%E)
	vecL2 	= Bst(vec2,0.0_rk,0.0_rk,vec2%Pz/vec2%E)
	vecOut 	= Bst(vecL1,vecL2%Px/vecL2%E,vecL2%Py/vecL2%E,0.0_rk)
end function

! Initialises an unpolarised spin-1 density matrix to a unit matrix.
subroutine initSDM(this)
implicit none
class(SpinDensityMatrix), intent(inout) :: this

    this%xx = 1.0_rk
    this%xy = 0.0_rk
    this%xz = 0.0_rk
    this%yx = 0.0_rk
    this%yy = 1.0_rk
    this%yz = 0.0_rk
    this%zx = 0.0_rk
    this%zy = 0.0_rk
    this%zz = 1.0_rk
end subroutine

! Initialises an unpolarised spin density matrix to the
! components specified.
subroutine initSDMElem(rho,xx,xy,xz,&
                        yx,yy,yz,zx,zy,zz)
implicit none
class(SpinDensityMatrix), intent(inout) :: rho
real(kind=rk), intent(in) :: xx, xy, xz
real(kind=rk), intent(in) :: yx, yy, yz
real(kind=rk), intent(in) :: zx, zy, zz

    rho%xx = xx
    rho%xy = xy
    rho%xz = xz
    rho%yx = yx
    rho%yy = yy
    rho%yz = yz
    rho%zx = zx
    rho%zy = zy
    rho%zz = zz
end subroutine

! Initialises to zero all elements of the
! spin density matrix.
subroutine nullSDM(rho)
implicit none
class(SpinDensityMatrix), intent(inout) :: rho

    rho%xx = 0.0_rk
    rho%xy = 0.0_rk
    rho%xz = 0.0_rk
    rho%yx = 0.0_rk
    rho%yy = 0.0_rk
    rho%yz = 0.0_rk
    rho%zx = 0.0_rk
    rho%zy = 0.0_rk
    rho%zz = 0.0_rk
end subroutine

! Print the elements of a spin density matrix.
subroutine printSDM(rho)
implicit none
class(SpinDensityMatrix), intent(inout) :: rho
   
    print*,'\n'
    print*, real(rho%xx), " ", real(rho%xy), " ", real(rho%xz)
    print*, real(rho%yx), " ", real(rho%yy), " ", real(rho%yz)
    print*, real(rho%zx), " ", real(rho%zy), " ", real(rho%zz)
    print*,'\n'
end subroutine


end module NewVariables
